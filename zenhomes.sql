/*
SQLyog Ultimate v8.82 
MySQL - 8.0.16 : Database - zenhomes_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`zenhomes_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `zenhomes_db`;

/*Table structure for table `contract` */

DROP TABLE IF EXISTS `contract`;

CREATE TABLE `contract` (
  `contract_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `landlord_id` bigint(20) NOT NULL,
  `rental_price` double NOT NULL,
  `start_from` datetime DEFAULT NULL,
  `end_on` datetime NOT NULL,
  PRIMARY KEY (`contract_id`),
  KEY `FK_contract` (`landlord_id`),
  CONSTRAINT `FK_contract` FOREIGN KEY (`landlord_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `contract` */

/*Table structure for table `contract_property` */

DROP TABLE IF EXISTS `contract_property`;

CREATE TABLE `contract_property` (
  `contract_id` bigint(20) NOT NULL,
  `property_id` bigint(20) NOT NULL,
  KEY `FK_contract_property` (`property_id`),
  KEY `FK_contract_property_contract` (`contract_id`),
  CONSTRAINT `FK_contract_property` FOREIGN KEY (`property_id`) REFERENCES `property` (`property_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_contract_property_contract` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`contract_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `contract_property` */

/*Table structure for table `contract_tenant` */

DROP TABLE IF EXISTS `contract_tenant`;

CREATE TABLE `contract_tenant` (
  `contract_id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) NOT NULL,
  KEY `FK_contract_tenant` (`tenant_id`),
  KEY `FK_contract_tenant_contract` (`contract_id`),
  CONSTRAINT `FK_contract_tenant` FOREIGN KEY (`tenant_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_contract_tenant_contract` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `contract_tenant` */

/*Table structure for table `property` */

DROP TABLE IF EXISTS `property`;

CREATE TABLE `property` (
  `property_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `landlord_id` bigint(20) NOT NULL,
  `type` enum('building','apartment') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `location` text COLLATE utf8_bin,
  `parent_property_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`property_id`),
  KEY `FK_user_property` (`landlord_id`),
  CONSTRAINT `FK_user_property` FOREIGN KEY (`landlord_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `property` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `real_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `id_type` enum('passport','citizen_card','residence_card') COLLATE utf8_bin DEFAULT NULL COMMENT 'National ID Card number',
  `id_number` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
